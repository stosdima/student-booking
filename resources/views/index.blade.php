@extends('layouts.main')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">List of all students</div>
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}">
                            {!! session('message.content') !!}
                        </div>
                    @endif
                    <div class="panel-body">
                        {{ Form::open(['url' =>'/search','method' => 'get', 'role' => 'search']) }}
                        <div class="form-group row">
                            <div class="col-xs-6">
                                {{ Form::label('search', 'Search:') }}
                                <div class="input-group">
                                    {{ Form::text('search', null, ['class' => 'form-control']) }}
                                    <span class="input-group-btn">
                                        {{ Form::submit('Search', ['class' => 'btn btn-info text-center']) }}
                                        <a class="btn btn-small btn-danger" href="{{url('/')}}">Reset</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>@sortablelink('name')</td>
                                <td>@sortablelink('surname')</td>
                                <td>@sortablelink('gr_number')</td>
                                <td>@sortablelink('points')</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($students as $student)
                                <tr>
                                    <td>{!! $student->name !!}</td>
                                    <td>{!! $student->surname !!}</td>
                                    <td>{!! $student->gr_number !!}</td>
                                    <td>{!! $student->points !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">{!! $students->appends(\Request::except('page'))->render() !!}</div>

                    </div>
                </div>
            </div>
@endsection