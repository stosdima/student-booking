@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">Edit profile</div>
                    <div class="panel-body">
                        {{ Form::model($student, ['route' => ['dashboard.update', $student->id], 'method'=> 'PUT', 'class' => 'form-horizontal']) }}
                        {{ csrf_field() }}
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {{Form::label('name', 'Name')}}
                                    {{Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus'])}}
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {{Form::label('surname', 'Surname')}}
                                    {{Form::text('surname', null, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus'])}}
                                    @if ($errors->has('surname'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('surname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {{Form::label('email', 'Email')}}
                                    {{Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus'])}}
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {{Form::label('sex', 'Sex')}}
                                    {{Form::select('sex', [null =>'Please select' ,'Male' ,'Female'], null, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus'])}}
                                    @if ($errors->has('sex'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('sex') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="col-md-6">

                            <div class="form-group{{ $errors->has('accomodation') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {{Form::label('accomodation', 'Accomodation')}}
                                    {{Form::select('accomodation', [null =>'Please select' ,'Local' ,'Nonresident'], null, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus'])}}
                                    @if ($errors->has('accomodation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('accomodation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('gr_number') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {{Form::label('gr_number', 'Group number')}}
                                    {{Form::text('gr_number', null, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus'])}}
                                    @if ($errors->has('gr_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('gr_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('points') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {{Form::label('points', 'Points')}}
                                    {{Form::text('points', null, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus'])}}
                                    @if ($errors->has('points'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('points') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('birth') ? ' has-error' : '' }}">
                                <div class="col-md-6">
                                    {{Form::label('birth', 'Birth')}}
                                    {{Form::date('birth', null, ['class' => 'form-control', 'required' => 'required'])}}
                                    @if ($errors->has('birth'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('birth') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-danger" href="{{ url('/dashboard') }}">Back</a>
                        {{ Form::submit('Update', ['class' => 'btn btn-success text-center']) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
@endsection