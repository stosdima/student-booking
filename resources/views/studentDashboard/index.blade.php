@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">Hello, {{$student->name}}</div>
                    @if(session()->has('message.level'))
                        <div class="alert alert-{{ session('message.level') }}">
                            {!! session('message.content') !!}
                        </div>
                    @endif
                    <div class="panel-body">
                        <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <td>Name</td>
                            <td>Surname</td>
                            <td>Email</td>
                            <td>Sex</td>
                            <td>Group number</td>
                            <td>Points</td>
                            <td>Birth</td>
                            <td>Accomodation</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$student->name}}</td>
                                <td>{{$student->surname}}</td>
                                <td>{{$student->email}}</td>
                                <td>{{$student->sex}}</td>
                                <td>{{$student->gr_number}}</td>
                                <td>{{$student->points}}</td>
                                <td>{{$student->birth}}</td>
                                <td>{{$student->accomodation}}</td>
                                <td><a class="btn btn-small btn-info" href="{{url('/dashboard/edit')}}">Edit</a></td>

                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
@endsection