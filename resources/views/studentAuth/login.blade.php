@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-info">
                    <div class="panel-heading">Student Login</div>
                    <div class="panel-body">
                        {{ Form::open(['url' =>'/login','method' => 'post', 'class' => 'form-horizontal']) }}
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                {{Form::label('email', 'Email')}}
                                {{Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'autofocus' => 'autofocus'])}}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                {{Form::label('password', 'Password')}}
                                {{Form::password('password', ['class' => 'form-control', 'required' => 'required'])}}
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                @endif
                            </div>
                        </div>
                        {{ Form::submit('Login', ['class' => 'btn btn-success']) }}
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection