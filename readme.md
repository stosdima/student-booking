### I. Versions : 
1. Laravel: 5.5 ; 
2. PHP: 7.1;
3. MYSQL: 5.7;
 
### II. Installation:
1. Clone project `git clone https://stosdima@bitbucket.org/stosdima/student-booking.git`
2. Download and install composer(https://getcomposer.org/)
3. Go to project root folder
4. Install dependencies from console: `composer install`
5. Chancge configs for DB (for convenience I did not delete `.env file`)
    1. DB_DATABASE=your_db_name
    2. DB_USERNAME=your_login
    3. DB_PASSWORD=your_password
6. Run command `php artisan migrate`
7. For filling Db use command `php artisan db:seed`  