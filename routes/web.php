<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@show');
Route::get('/search', 'IndexController@search');

Route::group(['middleware' => 'students_guest'], function (){
    Route::get('/register', 'StudentAuth\RegisterController@show');
    Route::post('/register', 'StudentAuth\RegisterController@register');
    Route::get('/login', 'StudentAuth\LoginController@show');
    Route::post('/login', 'StudentAuth\LoginController@login');
});

Route::group(['middleware' => 'students_auth'], function (){
    Route::get('/logout', 'StudentAuth\LoginController@logout');
    Route::get('/dashboard', 'DashboardController@show');
    Route::get('/dashboard/edit', 'DashboardController@edit');
    Route::put('/dashboard/edit', ['as' => 'dashboard.update', 'uses' => 'DashboardController@update']);
});
