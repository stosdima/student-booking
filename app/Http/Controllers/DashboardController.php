<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Students;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DashboardController extends Controller
{
    protected $redirectTo = '/login';

    public function show()
    {
        if ($this->guard()->check()) {
            $student = $this->guard()->user();
            return view('studentDashboard.index', ['student' => $student]);
        } else {
            return redirect($this->redirectTo);
        }
    }

    public function edit()
    {
        if ($this->guard()->check()) {
            $student = $this->guard()->user();
            return view('studentDashboard.edit', ['student' => $student]);
        } else {
            return redirect($this->redirectTo);
        }
    }

    public function update(Request $request)
    {
        if ($this->guard()->check()) {
            $this->validator($request->all())->validate();
            $studentId = $this->guard()->id();
            $student = Students::find($studentId);
            if ($student->update($request->all())) {
                $request->session()->flash('message.level', 'success');
                $request->session()->flash('message.content', 'Profile successfully updated');
                return redirect('/dashboard');
            } else {
                $request->session()->flash('message.level', 'danger');
                $request->session()->flash('message.content', 'Error!');
            }

        } else {
            return redirect($this->redirectTo);
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'max:255',
            'surname' => 'max:255',
            'gr_number' => 'min:2|max:5',
            'email' => 'email|max:255',
            'points' => 'numeric|min:100|max:200',
            'birth' => 'date',
        ]);
    }

    protected function guard()
    {
        return Auth::guard('web_students');
    }
}
