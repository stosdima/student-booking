<?php

namespace App\Http\Controllers;

use App\Students;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function show()
    {
        $students = Students::sortable()->orderBy('points', 'desc')
            ->paginate(25);
        return view('index')->with(['students' => $students]);
    }

    public function search(Request $request)
    {
        $searchWord = $request->get('search');
        $students = Students::sortable()->orderBy('points', 'desc')
            ->where('name', 'like', '%' . $searchWord . '%')
            ->orWhere('surname', 'like', '%' . $searchWord . '%')
            ->orWhere('gr_number', 'like', '%' . $searchWord . '%')
            ->orWhere('points', 'like', '%' . $searchWord . '%')
            ->paginate(25);
        foreach ($students as $student) {
            $student->name = $this->highlight($searchWord, $student->name);
            $student->surname = $this->highlight($searchWord, $student->surname);
            $student->gr_number = $this->highlight($searchWord, $student->gr_number);
            $student->points = $this->highlight($searchWord, $student->points);
        }
        return view('index')->with(['students' => $students]);
    }

    protected function highlight($search, $replace)
    {
        return preg_replace("/" . preg_quote($search, "/") . "/i", "<span style='color:red'>$0</span>", $replace);
    }

}
