<?php

namespace App\Http\Controllers\StudentAuth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/dashboard';

    protected function guard()
    {
        return Auth::guard('web_students');
    }

    public function show()
    {
        return view('studentAuth.login');
    }

}
