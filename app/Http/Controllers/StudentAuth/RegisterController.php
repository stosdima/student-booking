<?php

namespace App\Http\Controllers\StudentAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Students;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    protected $redirectPath = '/dashboard';

    public function show()
    {
        return view('studentAuth.registration');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $students = $this->create($request->all());
        if ($students) {
            $this->guard()->login($students);
            $request->session()->flash('message.level', 'success');
            $request->session()->flash('message.content', 'Thanks!You successfully registered. It`s your dahsboard ;)');
            return redirect($this->redirectPath);
        } else {
            $request->session()->flash('message.level', 'danger');
            $request->session()->flash('message.content', 'Error!');
        }
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'sex' => 'required',
            'gr_number' => 'required|min:2|max:5',
            'email' => 'required|email|max:255|unique:students',
            'password' => 'required|min:6|confirmed',
            'points' => 'numeric|required|min:100|max:200',
            'birth' => 'required|date',
            'accomodation' => 'required',
        ]);
    }

    protected function create(array $data)
    {
        return Students::create([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'sex' => $data['sex'],
            'gr_number' => $data['gr_number'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'points' => $data['points'],
            'birth' => $data['birth'],
            'accomodation' => $data['accomodation'],
        ]);

    }

    protected function guard()
    {
        return Auth::guard('web_students');
    }
}
