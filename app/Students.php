<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthTrait;
use Kyslik\ColumnSortable\Sortable;

class Students extends Model implements Authenticatable
{
    use AuthTrait;
    use Sortable;

    protected $table = 'students';
    protected $fillable = ['name', 'surname', 'sex', 'gr_number', 'points', 'email', 'birth', 'accomodation', 'password'];
    protected $hidden = ['remember_token'];
    public $sortable = ['name', 'surname', 'gr_number', 'points'];

}
