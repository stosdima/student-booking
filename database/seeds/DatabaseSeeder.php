<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range('50', '100') as $index)
        {
            DB::table('students')->insert([
                'name' => $faker->firstName,
                'surname' => $faker->lastName,
                'sex' => $faker->boolean,
                'gr_number' => rand(10, 1000). $faker->randomLetter,
                'email' => $faker->email,
                'password' => bcrypt($faker->password),
                'points' => rand(100, 200),
                'birth' => $faker->date(),
                'accomodation' => $faker->boolean
            ]);
        }
    }
}
